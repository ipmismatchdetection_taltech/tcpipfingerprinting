import socket
s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
s.bind(('127.0.0.1', 8080))

while True:
    frame, meta = s.recvfrom(1024)
    print("Frame:")
    print(len(frame))
    print(repr(frame))
    print("Meta:")
    print(len(meta))
    print(repr(meta))
    # TODO: Parse frames here